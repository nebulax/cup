
QUERY_PKG_SCRIPT="${SRC_DIR}"/conf_query_pkg.awk
GET_SECTION_SCRIPT="${SRC_DIR}"/conf_get_sections.awk

SECTION_NAME_PREPARE="prepare"

pkg_get_sections()
{
	local _conffile

	_conffile=$1

	awk -f "${GET_SECTION_SCRIPT}" "${_conffile}"
}

pkg_query_section()
{
	local _conffile _section _group

	_conffile=$1
	_section=$2
	_group=$3
	[ -n "${_group}" ] || _group="DEFAULT"

	awk -v section="${_section}" -v group="${_group}" -f "${QUERY_PKG_SCRIPT}" "${_conffile}"
}

if [ -z "${CONF_DIR}" ]
then
	if [ -d "${SRC_DIR}/../cup.conf.d" ]
	then
		CONF_DIR="${SRC_DIR}/../cup.conf.d"
	else
		CONF_DIR=""
	fi
fi
[ -z "${CONF_DIR}" ] || m_info "Using conf dir: " '\+bold' "${CONF_DIR}" '\-bold'
