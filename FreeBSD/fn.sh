
DISTRO_ID="freebsd"

pkg_update_info()
{
	pkg update
}

pkg_upgrade()
{
	ASSUME_ALWAYS_YES=yes pkg upgrade
}

pkg_install()
{
	local _pkgname

	_pkgname=$1

	ASSUME_ALWAYS_YES=yes pkg install ${_pkgname}
}

pkg_installed_version()
{
	local _pkgname _version

	_pkgname=$1
	_version=$(pkg query "%v" ${_pkgname} || :)

	echo ${_version}
}

pkg_target_version()
{
	local _pkgname _version

	_pkgname=$1
	_version=$(pkg rquery "%v" ${_pkgname} || :)

	echo ${_version}
}
