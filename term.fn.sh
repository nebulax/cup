
ESCAPE_CODE_FILE="${SRC_DIR}/term.escape.codes"

_get_escape_code()
{
	local _attr

	_attr=$1
	echo $(awk -v attr="${_attr}" -F'=' '$1 == attr {print $2}' "${ESCAPE_CODE_FILE}")
}

_get_escape_str()
{
	local _str _code
	local -n _eattrs=$1

	_str="\e[0"
	for _attr in "${_eattrs[@]}"
	do
		_code=$(_get_escape_code "${_attr}")
		[ -z "${_code}" ] || _str="${_str};${_code}"
	done
	_str="${_str}m"
	printf "%s" "${_str}"
}

_push_attrs()
{
	local _attr _anum
	local -n _pattrs=$1

	_attr=$2

	_anum=${#_pattrs[@]}
	_pattrs[${_anum}]="${_attr}"
}

_pop_attrs()
{
	local _mattr _anum _i _j
	local -n _pattrs=$1

	_mattr=$2

	_anum=${#_pattrs[@]}
	_i=1
	while [ "${_i}" -le "${_anum}" ]
	do
		_j=$((_anum - _i))
		_i=$((_i + 1))

		if [ "${_pattrs[${_j}]}" = "${_mattr}" ]
		then
			unset _pattrs[${_j}]
			return
		fi
	done
}

_modify_attrs()
{
	local _attr _c1
	local -n _mattrs=$1

	_attr=$2

	_c1=$(printf "%s" "${_attr}" | sed -n 's/^\(.\).*/\1/p')
	_attr=$(printf "%s" "${_attr}" | sed -n "s/^.\(.*\)/\1/p")
	case ${_c1} in
	'+')
		_push_attrs _mattrs "${_attr}"
		;;
	'-')
		_pop_attrs _mattrs "${_attr}"
		;;
	*)
		echo "Invalid attribute ${_attr}" >&2
		;;
	esac
}

_param_get_pattern()
{
	local _p _c1 _c2

	_p=$1

	_c1=$(printf "%s" "${_p}" | sed -n 's/^\(.\).*/\1/p')
	if [ "${_c1}" != '\' ]
	then
		printf "%s" "%s"
		return
	fi

	_c2=$(printf "%s" "${_p}" | sed -n 's/^[\]\(.\).*/\1/p')
	case ${_c2} in
	'+'|'-')
		return
		;;
	'n')
		printf "%s" '\n'
		return
		;;
	'\')
		;;
	*)
		;;
	esac

	printf "%s" "%s"
}

_param_get_value()
{
	local _p _c1 _c2

	_p=$1

	_c1=$(printf "%s" "${_p}" | sed -n 's/^\(.\).*/\1/p')
	if [ "${_c1}" != '\' ]
	then
		printf "%s" "${_p}"
		return
	fi

	_c2=$(printf "%s" "${_p}" | sed -n 's/^[\]\(.\).*/\1/p')
	case ${_c2} in
	'+'|'-')
		return
		;;
	'n')
		return
		;;
	'\')
		printf "%s" "${_p}" | sed -n "s/^[\]\(.*\)/\1/p"
		return
		;;
	*)
		;;
	esac

	printf "%s" "${_p}"
}

_param_get_attr()
{
	local _p _c1 _c2

	_p=$1

	_c1=$(printf "%s" "${_p}" | sed -n 's/^\(.\).*/\1/p')
	if [ "${_c1}" != '\' ]
	then
		return
	fi

	_c2=$(printf "%s" "${_p}" | sed -n 's/^[\]\(.\).*/\1/p')
	case ${_c2} in
	'+'|'-')
		printf "%s" "${_p}" | sed -n "s/^[\]\(.*\)/\1/p"
		return
		;;
	'n')
		;;
	'\')
		;;
	*)
		;;
	esac
}

_msg_print()
{
	local _p _pattern _vnum _i _p_attr _p_pattern _p_value
	local -a _values
	local -a _attrs

	_pattern=""
	_vnum=0

	_i=1
	while [ "${_i}" -le "$#" ]
	do
		_p=$(eval "printf %s \"\$${_i}\"")
		_i=$((_i + 1))

		_p_attr=$(_param_get_attr "${_p}")
		_p_pattern=$(_param_get_pattern "${_p}")
		_p_value=$(_param_get_value "${_p}")
#		echo "#$_i: $_p"
#		echo "a: ${_p_attr}, p: ${_p_pattern}, v: ${_p_value}"

		if [ -n "${_p_attr}" ]
		then
			_modify_attrs _attrs "${_p_attr}"
			_p_pattern=$(_get_escape_str _attrs)
		fi

		if [ -n "${_p_pattern}" ]
		then
			_pattern="${_pattern}${_p_pattern}"
		fi

		if [ -n "${_p_value}" ]
		then
			_values[${_vnum}]="${_p_value}"
			_vnum=$((_vnum + 1))
		fi
	done
	printf "${_pattern}\n" "${_values[@]}"
}

m_print()
{
	_msg_print "$@"
}

m_error()
{
	_msg_print '\+red' "$@" '\+reset'
}

m_notice()
{
	_msg_print '\+green' "$@" '\+reset'
}

m_info()
{
	_msg_print '\+reset' "$@" '\+reset'
}

m_warn()
{
	_msg_print '\+yellow' "$@" '\+reset'
}
