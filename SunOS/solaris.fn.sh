
pkg_update_info()
{
	pkg refresh -q
}

pkg_upgrade()
{
	pkg update -q --accept --no-refresh
}

pkg_install()
{
	local _pkgname

	_pkgname=$1

	pkg install -q --accept --no-refresh /${_pkgname}@latest
}

pkg_installed_version()
{
	local _pkgname _version

	_pkgname=$1
	_version=$(LANG=en_US.UTF-8 pkg info -l /${_pkgname} | awk '$1 == "Version:" {print $2}')

	echo ${_version}
}

pkg_target_version()
{
	local _pkgname _version

	_pkgname=$1
	_version=$(LANG=en_US.UTF-8 pkg info -r /${_pkgname}@latest | awk '$1 == "Version:" {print $2}')

	echo ${_version}
}
