
BEGIN {
	group_found = 0
	section_found = 0
}

$1 == "COMMON:" {
	group_found = 1
	next
}

$1 == group":" {
	group_found = 1
	next
}

$1 == "--" {
	group_found = 0
	section_found = 0
	next
}

$1 == "@"section":" {
	if (group_found) {
		section_found = 1
	}
	next
}

$1 == "-" && NF == 1 {
	section_found = 0
	next
}

$1 == "-" && NF > 1 {
	if (section_found) {
		print $2
	}
}
