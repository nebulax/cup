
DISTRO_ID="netbsd"

pkg_update_info()
{
	pkgin update
}

pkg_upgrade()
{
	pkgin -y full-upgrade
}

pkg_install()
{
	local _pkgname

	_pkgname=$1

	pkg -y install ${_pkgname}
}

pkg_installed_version()
{
	local _pkgname _version

	_pkgname=$1
	_version=$(pkg_info -X ${_pkgname} | awk -F'=' -v name=${_pkgname} '$1 == "PKGNAME" {print substr($2, length(name)+2)}' || :)

	echo ${_version}
}

pkg_target_version()
{
	local _pkgname _version

	_pkgname=$1
	_version=$(pkgin search "^${_pkgname}$" | awk -v name=${_pkgname} 'NR == 1 {print substr($1, length(name)+2)}'|| :)

	echo ${_version}
}
