
BEGIN {
	print_section = 0
}

$1 == "SECTIONS:" {
	print_section = 1
	next
}

$1 == "--" {
	print_section = 0
	next
}

$1 == "-" && NF == 2 {
	if (print_section && $2 != "prepare") { # prepare is a spectial section
		print $2
	}
}
